cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20220106 — Bump SMTK and ParaView for new widgets.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "61d6e77d4acac99f42b7d2c9")
  set(file_hash "780059fd003142b98d8d3a5f062785a7839a89d566fdecf4fb62a6766e5e8009fc6048cdcb5f0cbc6667194d3f5aac798091e9583ffa649dea9edda3b8d94576")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_item "61d79a594acac99f42c05904")
  set(file_hash "b39e7ffdcd9d143e437f8405f3b3f8bafae1679f2f37326b54c0f32901556e56527fa9e2801ebb7fe2a97908a34142f18dd75953a04be51da420158676ca6fe2")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
