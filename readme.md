## SMTK session for aeva

The aeva session is focused on annotating virtual anatomy
and transforming those annotations across different models
of the same anatomical features — whether they be models
of the same patient but at different points in time like a
longitudinal study; or models of different patients in a
cohort; or abstracted models of common nominal and/or
malformed modes.

+ For more information, see [aeva's website at simtk.org](https://simtk.org/projects/aeva-apps)
+ For application binaries that use this session,
  see [aeva downloads at data.kitware.com](https://data.kitware.com/#collection/5e387f9caf2e2eed3562242e)
    + `custom` contains custom builds
    + `continuous` contains builds queued by our continuous integration system.

+ Build and develop documentation can be found in the [aeva repository](https://gitlab.kitware.com/aeva/aeva/-/tree/master/doc/dev)
