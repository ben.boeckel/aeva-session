//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_NameManager_h
#define smtk_session_aeva_NameManager_h

#include "smtk/session/aeva/Exports.h"

#include <memory>
#include <string>

namespace smtk
{
namespace resource
{
class PersistentObject;
}
namespace session
{
namespace aeva
{

/**\brief An application-wide utility for assigning unique names to objects.
  *
  */
class SMTKAEVASESSION_EXPORT NameManager
{
public:
  NameManager();
  ~NameManager() = default;

  using Ptr = std::shared_ptr<NameManager>;

  static Ptr create();

  std::string nameForObject(const smtk::resource::PersistentObject& obj);

protected:
  int m_counter{0};
};

}
}
}
#endif
