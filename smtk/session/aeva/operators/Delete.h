//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_Delete_h
#define smtk_session_aeva_Delete_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

#include "vtkImageData.h"
#include "vtkSmartPointer.h"

namespace smtk
{
namespace session
{
namespace aeva
{

class SMTKAEVASESSION_EXPORT Delete : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::Delete);
  smtkCreateMacro(Delete);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

  bool ableToOperate() override;

  void setSuppressOutput(bool suppress) { m_suppressOutput = suppress; }
  bool suppressOutput() const { return m_suppressOutput; }

protected:
  Delete();
  Result operateInternal() override;
  void generateSummary(smtk::operation::Operation::Result& result) override;
  const char* xmlDescription() const override;

  Result m_result;
  std::vector<smtk::common::UUID> m_preservedUUIDs;
  bool m_suppressOutput{false};
};

}
}
}

#endif
