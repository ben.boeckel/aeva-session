//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_EditFreeformAttributes_h
#define smtk_session_aeva_EditFreeformAttributes_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/// Set (or remove) an attribute value on a set of entities. The string, integer,
/// and floating-point values are all optional. Any combination may be specified.
/// All that are specified are set; those unspecified are removed.
class SMTKAEVASESSION_EXPORT EditFreeformAttributes : public Operation
{
public:
  smtkTypeMacro(smtk::session::aeva::EditFreeformAttributes);
  smtkSharedPtrCreateMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::session::aeva::Operation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

}
}
}

#endif
