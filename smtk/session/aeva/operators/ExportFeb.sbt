<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA "ExportFeb" Operator -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export feb" Label="FEBio - Export Resource" BaseType="operation">
      <AssociationsDef Name="Resource(s)" NumberOfRequiredValues="1"
                       Extensible="true" OnlyResources="true">
        <!-- TODO should include new property filter string for "aeva_datatype" -->
        <Accepts><Resource Name="smtk::session::aeva::Resource"/></Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <File Name="filename" NumberOfRequiredValues="1" Extensible="true"
          ShouldExist="false"
          FileFilters="FEBio Data (*.feb)">
        </File>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(exportfeb)" BaseType="result">
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
