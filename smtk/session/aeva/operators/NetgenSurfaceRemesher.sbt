<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "netgen surface remesher" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="netgen surface remesher" Label="Netgen Surface Remesher" BaseType="operation">

      <BriefDescription>Generate a quadratic mesh promoted from a linear mesh.</BriefDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="max global mesh size" Label="Max Global Mesh Size">
          <BriefDescription>Maximum global mesh size allowed</BriefDescription>
          <DefaultValue>1.0e6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>

        <Int Name="optimize step" Label="Number of Optimize Steps">
          <BriefDescription>Number of optimize steps to use for 3-D mesh optimization</BriefDescription>
          <DefaultValue>3</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(netgen surface remesher)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
