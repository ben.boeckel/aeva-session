//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_SmoothSurface_h
#define smtk_session_aeva_SmoothSurface_h

#include "smtk/session/aeva/Operation.h"
#include "smtk/session/aeva/Resource.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/**\brief Smooth a surface using a windowed sinc function interpolation kernel
  *
  * For each vertex v, a topological and geometric analysis is
  * performed to determine which vertices are connected to v, and
  * which cells are connected to v. Then, a connectivity array is
  * constructed for each vertex. (The connectivity array is a list of lists
  * of vertices that directly attach to each vertex.) Next, an iteration
  * phase begins over all vertices. For each vertex v, the coordinates of v
  * are modified using a windowed sinc function interpolation kernel.
  * Taubin describes this methodology in the IBM tech report:
  *
  * RC-20404 (#90237, dated 3/12/96) "Optimal Surface Smoothing as Filter Design"
  * G. Taubin, T. Zhang and G. Golub.
  *
  * \sa vtkWindowedSincPolyDataFilter
  */
class SMTKAEVASESSION_EXPORT SmoothSurface : public Operation
{

public:
  smtkTypeMacro(smtk::session::aeva::SmoothSurface);
  smtkCreateMacro(SmoothSurface);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_SmoothSurface_h
