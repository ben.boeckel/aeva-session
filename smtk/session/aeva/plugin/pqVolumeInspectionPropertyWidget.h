//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_aeva_pqVolumeInspectionPropertyWidget_h
#define smtk_session_aeva_pqVolumeInspectionPropertyWidget_h

#include "pqInteractivePropertyWidget.h"

class pqColorChooserButton;

class pqVolumeInspectionPropertyWidget : public pqInteractivePropertyWidget
{
  Q_OBJECT
  using Superclass = pqInteractivePropertyWidget;

public:
  pqVolumeInspectionPropertyWidget(vtkSMProxy* proxy,
    vtkSMPropertyGroup* smgroup,
    QWidget* parent = nullptr);
  ~pqVolumeInspectionPropertyWidget() override;

  using Superclass::addPropertyLink;
  void addPropertyLink(pqColorChooserButton* color, const char* propertyName, int smindex = -1);

public Q_SLOTS: // NOLINT(readability-redundant-access-specifiers)
  void pickOrigin(double wx, double wy, double wz); //center point

protected Q_SLOTS:
  void placeWidget() override;

protected: // NOLINT(readability-redundant-access-specifiers)
  class Internals;
  Internals* m_p;

private:
  Q_DISABLE_COPY(pqVolumeInspectionPropertyWidget);
};

#endif //smtk_session_aeva_pqVolumeInspectionPropertyWidget_h
