//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_aeva_Geometry_h
#define pybind_smtk_session_aeva_Geometry_h

#include <pybind11/pybind11.h>

#include "smtk/session/aeva/Geometry.h"

namespace py = pybind11;

inline PySharedPtrClass< smtk::session::aeva::Geometry, smtk::geometry::Cache<smtk::extension::vtk::geometry::Geometry> > pybind11_init_smtk_session_aeva_Geometry(py::module &m)
{
  PySharedPtrClass< smtk::session::aeva::Geometry, smtk::geometry::Cache<smtk::extension::vtk::geometry::Geometry> > instance(m, "Geometry");
  instance
    .def(py::init<::std::shared_ptr<smtk::session::aeva::Resource> const &>())
    .def("typeName", &smtk::session::aeva::Geometry::typeName)
    .def("resource", &smtk::session::aeva::Geometry::resource)
    .def("queryGeometry", &smtk::session::aeva::Geometry::queryGeometry, py::arg("obj"), py::arg("entry"))
    .def("dimension", &smtk::session::aeva::Geometry::dimension, py::arg("obj"))
    .def("purpose", &smtk::session::aeva::Geometry::purpose, py::arg("obj"))
    .def("update", &smtk::session::aeva::Geometry::update)
    .def("geometricBounds", &smtk::session::aeva::Geometry::geometricBounds, py::arg("arg0"), py::arg("bbox"))
    .def_readonly_static("type_name", &smtk::session::aeva::Geometry::type_name)
    ;
  return instance;
}

#endif
