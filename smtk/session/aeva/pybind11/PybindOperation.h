//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_aeva_Operation_h
#define pybind_smtk_session_aeva_Operation_h

#include <pybind11/pybind11.h>

#include "smtk/session/aeva/Operation.h"

namespace py = pybind11;

inline PySharedPtrClass< smtk::session::aeva::Operation, smtk::operation::XMLOperation > pybind11_init_smtk_session_aeva_Operation(py::module &m)
{
  PySharedPtrClass< smtk::session::aeva::Operation, smtk::operation::XMLOperation > instance(m, "Operation");
  instance
    // .def("deepcopy", (smtk::session::aeva::Operation & (smtk::session::aeva::Operation::*)(::smtk::session::aeva::Operation const &)) &smtk::session::aeva::Operation::operator=)
    .def_static("storage", (vtkSmartPointer<vtkDataObject> (*)(::smtk::resource::PersistentObject const *)) &smtk::session::aeva::Operation::storage, py::arg("arg0"))
    .def_static("storage", (vtkSmartPointer<vtkDataObject> (*)(::std::shared_ptr<smtk::resource::PersistentObject> const &)) &smtk::session::aeva::Operation::storage, py::arg("arg0"))
    .def_static("fetchResourceAndSession", &smtk::session::aeva::Operation::fetchResourceAndSession, py::arg("assoc"), py::arg("resource"), py::arg("session"))
    ;
  return instance;
}

#endif
