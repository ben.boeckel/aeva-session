//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_aeva_Version_h
#define pybind_smtk_session_aeva_Version_h

#include <pybind11/pybind11.h>

#include "smtk/session/aeva/Version.h"

namespace py = pybind11;

inline py::class_< smtk::session::aeva::Version > pybind11_init_smtk_session_aeva_Version(py::module &m)
{
  py::class_< smtk::session::aeva::Version > instance(m, "Version");
  instance
    .def(py::init<>())
    .def(py::init<::smtk::session::aeva::Version const &>())
    .def("deepcopy", (smtk::session::aeva::Version & (smtk::session::aeva::Version::*)(::smtk::session::aeva::Version const &)) &smtk::session::aeva::Version::operator=)
    // Note that major() and minor() are posix keywords, and castxml outputs some odd syntax on linux (gnu_dev_major).
    // So manually define major() and minor() here with lambda expressions.
    .def_static("major", []{return smtk::session::aeva::Version::major();})
    .def_static("minor", []{return smtk::session::aeva::Version::minor();})
    .def_static("patch", &smtk::session::aeva::Version::patch)
    .def_static("number", &smtk::session::aeva::Version::number)
    .def_static("combined", (int (*)()) &smtk::session::aeva::Version::combined)
    .def_static("combined", (int (*)(int, int, int)) &smtk::session::aeva::Version::combined, py::arg("major"), py::arg("minor"), py::arg("patch"))
    ;
  return instance;
}

#endif
