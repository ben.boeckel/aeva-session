//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef vtk_aeva_ext_vtkMedWriter_h
#define vtk_aeva_ext_vtkMedWriter_h

#include "vtk/aeva/ext/AEVAExtModule.h"
#include "vtkMultiBlockDataSetAlgorithm.h"

#include <string>

class vtkMultiBlockDataSet;

// vtkMedWriter writes N meshes to a med file when provided with a valid
// vtkMultiBlockDataSet of vtkUnstructuredGrids
// per cell type as well as all the groups separately
// These groups come in a vtkMultiBlockDataSet as vtkUnstructuredGrids.
class AEVAEXT_EXPORT vtkMedWriter : public vtkMultiBlockDataSetAlgorithm
{
public:
  static vtkMedWriter* New();
  vtkTypeMacro(vtkMedWriter, vtkMultiBlockDataSetAlgorithm);

  vtkMedWriter(const vtkMedWriter&) = delete;
  vtkMedWriter& operator=(const vtkMedWriter&) = delete;

public:
  void PrintSelf(ostream& os, vtkIndent indent) override;

  //@{
  /**
    * The file to write too
    */
  vtkSetMacro(FileName, std::string);
  vtkGetMacro(FileName, std::string);
  //@}

protected:
  vtkMedWriter()
  {
    SetNumberOfOutputPorts(0);
    SetNumberOfInputPorts(1);
  }

  int RequestData(vtkInformation* request,
    vtkInformationVector** inputVec,
    vtkInformationVector* outputVec) override;

  int FillInputPortInformation(int port, vtkInformation* info) override;

  std::string FileName;
};

#endif
